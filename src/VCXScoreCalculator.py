import sys
import argparse
import os
import json
import xml.etree.ElementTree as ET
import math
import glob
import csv
import numpy as np
import matplotlib.pyplot as plt
from datetime import date

invalid = '<>:"/\|?*'

def create_parser():
    parser = argparse.ArgumentParser(description='Calculate VCX Score')
    parser.add_argument('folder', type=str, help='Folder where TE42 xmls are')
    parser.add_argument('--config', type=str, help='Config file', default='vcx2.0_config.json')
    parser.add_argument('--plotfunctions', action='store_true', help='Plot functions of all metrics and save them as png files.')
    return parser

def read_from_xml(root, xml_entry):
    child = root.find(".//" + xml_entry)
    if child is None:
        raise Exception("XML entry not found.")
    return child.text

def normalizeToNyquist(mtf, nyquist):
    if mtf > nyquist:
        mtf = nyquist
    elif mtf < 0:
        mtf = nyquist
    return mtf

def get_value_from_xml(xml_entry, xmlFile, valueType):
    
    try:
        root = ET.parse(xmlFile)
        
        # Simple floating point value
        if valueType == "float":
            return float(read_from_xml(root, xml_entry))
        # Maximum of table
        elif valueType == "max":
            data = read_from_xml(root, xml_entry).split(";")
            data = list(filter(None, data))
            data = [float(a) for a in data]
            return max(data)
        # Calculate TPC
        elif valueType == "TPC":
            value1 = float(read_from_xml(root, xml_entry[0]))
            value2 = float(read_from_xml(root, xml_entry[1]))
            return (value1*value2)/1000000
        # Calculate max from two tables, report mean of them
        elif valueType == "maxMean":
            data1 = read_from_xml(root, xml_entry[0]).split(";")
            data1 = list(filter(None, data1))
            data1 = [float(a) for a in data1]
            maxdata1 = max(data1)
            data2 = read_from_xml(root, xml_entry[1]).split(";")
            data2 = list(filter(None, data2))
            data2 = [float(a) for a in data2]
            maxdata2 = max(data2)
            return (maxdata1+maxdata2)/2
        # Calculate ratio between two entries
        elif valueType == "ratio":
            value1 = float(read_from_xml(root, xml_entry[0]))
            value2 = float(read_from_xml(root, xml_entry[1]))
            return np.abs(value1)/np.abs(value2)
        # Get nth entry from table
        elif valueType == "entryFromTable":
            data = read_from_xml(root, xml_entry[0]).split(";")
            data = list(filter(None, data))
            data = [float(a) for a in data]
            return data[xml_entry[1]]
        # Mean of multiple entries from single xml
        elif valueType == "multiXMLEntryMean":
            data = list()
            for entry in xml_entry:
                data.append(float(read_from_xml(root, entry)))
            return sum(data)/len(data)
        # Calculate means of entries except last. Normalize result with last entry.
        elif valueType == "overshoot":
            data = list()
            for entry in xml_entry[:-1]:
                data.append(float(read_from_xml(root, entry)))
            result = sum(data)/len(data)
            result = (100*result)/float(read_from_xml(root, xml_entry[-1]))
            return result
        elif valueType == "overshootmax":
            data = list()
            for entry in xml_entry[:-1]:
                data.append(float(read_from_xml(root, entry)))
            result = max(data)
            result = (100*result)/float(read_from_xml(root, xml_entry[-1]))
            return result
        # Effective pixel count
        # Calculated from siemens star MTF10 and pixel count
        elif valueType == "EPC":
            nyquist = float(read_from_xml(root, xml_entry[-2]))
            pixelCount = float(read_from_xml(root, xml_entry[-1]))
            center = list()
            for i in range(8):
                value = float(read_from_xml(root, xml_entry[i]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                center.append(value)
            corner = list()
            #Corner 1
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+8]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 2
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+11]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 3
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+14]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 4
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+17]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            
            center = sum(center)/len(center)
            corner = sum(corner)/len(corner)
            
            return (((center+corner)/(2*nyquist))**2)*pixelCount
                    # Effective pixel count
        # Calculated from siemens star MTF10 and pixel count
        elif valueType == "EPCTPC":
            nyquist = float(read_from_xml(root, xml_entry[-2]))
            pixelCount = float(read_from_xml(root, xml_entry[-1]))
            center = list()
            for i in range(8):
                value = float(read_from_xml(root, xml_entry[i]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                center.append(value)
            corner = list()
            #Corner 1
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+8]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 2
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+11]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 3
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+14]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            #Corner 4
            for i in range(3):
                value = float(read_from_xml(root, xml_entry[i+17]))
                # Normalize orthogonal sectors to nyquist and diagonal to 1.4 * nyquist
                if i % 2 == 0:
                    value = normalizeToNyquist(value, nyquist)
                else:
                    value = normalizeToNyquist(value, nyquist*1.4)
                corner.append(value)
            
            center = sum(center)/len(center)
            corner = sum(corner)/len(corner)
            
            EPC = (((center+corner)/(2*nyquist))**2)*pixelCount
            TPC = pixelCount
            
            return (EPC/TPC)
            
     
            # Artifact metric which is calculated from dead leaves chart
        elif valueType == "artifacts":
            DL_cross = float(read_from_xml(root, xml_entry[0]))/100
            DL_direct_old = float(read_from_xml(root, xml_entry[1]))/100
            artifacts = 100-(DL_cross/(DL_direct_old/100))
            if artifacts < 0:
                return 0
            else:
                return artifacts
        # Floating point value, which is normalized to nyquist before reporting
        elif valueType == "normalizeNyquist":
            value = float(read_from_xml(root, xml_entry[0]))
            nyquist = float(read_from_xml(root, xml_entry[1]))
            return normalizeToNyquist(value, nyquist)
       # Floating point value, which is relative to nyquist 
        elif valueType == "rel2Nyquist":
            value = float(read_from_xml(root, xml_entry[0]))
            nyquist = float(read_from_xml(root, xml_entry[1]))
            return value/nyquist
        # Mean value of certain skin tone color patches
        elif valueType == "skinMean":
            patchNos = [4,9,62,63,64,65,66,67,68,74,75,76,77,78,79,80,92]
            patches = read_from_xml(root, xml_entry).split(";")
            
            mean = 0.0
            for number in patchNos:
                mean += float(patches[number])
            mean /= len(patchNos)
            
            return mean
        # Extract exif data from xml
        elif valueType == "exif":
            try:
                iso = read_from_xml(root, "Meta/ISO")
                et = float(read_from_xml(root, "Meta/ExpTime"))
                fno = read_from_xml(root, "Meta/Aperture")
                return "[ISO " + iso + " - f/" + fno + " - 1/" + str(int(1/et)) + "s]*"
            except:
                return "[N/A]"
        # String from xml
        elif valueType == "string":
            return read_from_xml(root, xml_entry)
        elif valueType == "AverageC":
            patchNos = [3,4,5,6,7,8,15,16,17,18,19,20,27,28,29,30,31,32,33]
            patchesa = read_from_xml(root, xml_entry + "/a/im").split(";")
            patchesb = read_from_xml(root, xml_entry + "/b/im").split(";")
            
            mean = 0.0
            for number in patchNos:
                a = float(patchesa[number])
                b = float(patchesb[number])
                C = math.sqrt(a*a+b*b)
                mean += C
            mean /= len(patchNos)
            
            return mean

        # Only for debugging purposes
        elif valueType == "debug":
            return 2.2
        else:
            sys.exit("Unknown value type.")
    except:
        print("XML entry not found " + xml_entry)

def find_subScoreFolder(folder, testcase):
    folders = [x[0] for x in os.walk(folder)]
    for folder in folders:
        if testcase in folder:
            return folder
    print("Cannot find IQAnalyzer xml for testcase " + testcase)
    return ""

def calculateScoreV20(value, formula, LGC, HGC, weight):
    if formula == "logarithmic":
        if value < LGC:
            result = 0
        elif value > HGC:
            result = 1
        else:
            result = math.log(value-LGC+1,HGC-LGC+1)
    elif formula == "flat_roof":
        if abs(value) > LGC:
            result = math.log(abs(value-LGC),LGC)*(-2)
            return result
        elif abs(value) < HGC:
            result = 1
        else:
            result = (LGC-abs(value))/(LGC-HGC)
    elif formula == "logarithmic_neg.linear":
        if value < 2*LGC-HGC:
            result = -1
        elif value < LGC:
            result = (LGC-value)/(LGC-HGC)
        elif value > HGC:
            result = 1
        else:
            result = math.log(value-LGC+1,HGC-LGC+1)
    elif formula == "roof_negative_ll":
        if value < 2*LGC-HGC or value > 3*HGC - 2*LGC:
            result = -1
        else:
            result = 1-abs(value-HGC)/(HGC-LGC)
    elif formula == "linear":
        if value < LGC:
            result = math.log(abs(value-LGC),LGC)*(-1)
            return result
        elif value > HGC:
            result = 1
        else:
            result = (LGC-value)/(LGC-HGC)
    elif formula == "roof_hl":
        if value < 2*HGC-LGC or value > LGC:
            result = math.log(abs(value-LGC),LGC)*(-1)
            return result
        else:
            result = 1-abs(value-HGC)/(LGC-HGC)
    elif formula == "logarithmic_roof":
        if abs(value) > LGC:
            result = math.log(abs(value-LGC),LGC)*(-2)
        elif abs(value) < HGC:
            result = 1
        else:
            result = 1 - math.log(abs(value)-HGC+1,LGC-HGC+1)
    elif formula == "roof_II":
        if value < LGC or value > 2*HGC-LGC:
            result = abs(value-LGC)*(-2)
            return result
        else:
            result = 1-abs(value-HGC)/(HGC-LGC)
    else:
        sys.exit("Formula not supported.")
    result *= weight
    return result

def calculateScoreV15(value, formula, LGC, HGC, weight):
    if formula == "logarithmic":
        if value < LGC:
            result = 0
        elif value > HGC:
            result = 1
        else:
            result = math.log(value-LGC+1,HGC-LGC+1)
    elif formula == "flat_roof":
        if abs(value) > LGC:
            result = 0
        elif abs(value) < HGC:
            result = 1
        else:
            result = (LGC-abs(value))/(LGC-HGC)
    elif formula == "logarithmic_neg.linear":
        if value < 2*LGC-HGC:
            result = -1
        elif value < LGC:
            result = (LGC-value)/(LGC-HGC)
        elif value > HGC:
            result = 1
        else:
            result = math.log(value-LGC+1,HGC-LGC+1)
    elif formula == "roof_negative_ll":
        if value < 2*LGC-HGC or value > 3*HGC - 2*LGC:
            result = -1
        else:
            result = 1-abs(value-HGC)/(HGC-LGC)
    elif formula == "linear":
        if value < LGC:
            result = 0
        elif value > HGC:
            result = 1
        else:
            result = (LGC-value)/(LGC-HGC)
    elif formula == "roof_hl":
        if value < 2*HGC-LGC or value > LGC:
            result = 0
        else:
            result = 1-abs(value-HGC)/(LGC-HGC)
    elif formula == "logarithmic_roof":
        if abs(value) > LGC:
            result = 0
        elif abs(value) < HGC:
            result = 1
        else:
            result = 1 - math.log(abs(value)-HGC+1,LGC-HGC+1)
    elif formula == "roof_II":
        if value < LGC or value > 2*HGC-LGC:
            result = 0
        else:
            result = 1-abs(value-HGC)/(HGC-LGC)
    else:
        sys.exit("Formula not supported.")
    result *= weight
    return result

def calculateScore(value, xp, yp, weight):
    score = np.interp(value, xp, yp)
    return score*weight

def calculateFinalSubScore(metrics):
    scoreSum = 0
    weightSum = 0
    for name, metric in metrics.items():
        scoreSum += metric["MetricScore"]
        weightSum += metric["Weight"]
    if weightSum == 0:
        return -1
    return (scoreSum/weightSum)*100

def calculateMeanFileSize(folder):
    files = glob.glob(folder + "\\*.jpg")

    if len(files) == 0:
        print("No images found in folder " + folder)
        return -1

    fileSizes = list()
    
    Keywords = ["check", "calc", "test", "analysis"]
    
    # Discard files that include certain keywords. These are IQAnalyzer analysis files.
    files = [s for s in files if not any(k in s for k in Keywords)]
    
    for file in files:
        fileSizes.append(os.path.getsize(file)/1024)
    return sum(fileSizes)/len(fileSizes)

def calculatePerformanceMetric(metric, folders):
    if metric["valueType"] == "debug":
        return 1.4

    if metric["valueType"] == "compressionLoss":
        framerateFolder = args.folder + "\\" + folders[metric["fileTag"][0]]
        AF800lxFolder = args.folder + "\\" + folders[metric["fileTag"][1]]

        framerateImageSize = calculateMeanFileSize(framerateFolder)
        AFImageSize = calculateMeanFileSize(AF800lxFolder)

        if framerateImageSize == -1 or AFImageSize == -1:
            return -1
        
        return ((framerateImageSize/AFImageSize)-1)*100
    
    if metric["valueType"] == "afFailure":
        folder = os.path.join(args.folder, *folders[metric["fileTag"][0]].split("\\"))
        xmls = glob.glob(os.path.join(folder, "*.xml"))
        
        MTFs = list()
        for xml in xmls:
            meanMTF = get_value_from_xml("DL_cross/Y/Results/Limit/MTF50", xml, "float")
            MTFs.append(meanMTF)

        # Get max from reference images xml
        folder_ref = os.path.join(args.folder, *folders[metric["fileTag"][1]].split("\\"))
        xmls_ref = glob.glob(os.path.join(folder_ref, "*.xml"))
        RefMTF = 0
        for xml in xmls_ref:
            meanMTF = get_value_from_xml("DL_cross/Y/Results/Limit/MTF50", xml, "float")
            if meanMTF > RefMTF:
                RefMTF = meanMTF

        # Threshold for blurriness
        threshold = metric["threshold"]
        
        unsharpCount = 0
        count = 0
        for MTF in MTFs:
            if RefMTF / abs(MTF) < threshold:
                unsharpCount += 1
            count += 1
            if count == metric["imageCount"]:
                break
            
        unsharpCount += metric["imageCount"] - count

        return (float(unsharpCount)/float(metric["imageCount"]))*100
        
    if metric["valueType"] == "delta":
        # Read result csv
        files = glob.glob(os.path.join(args.folder, "*" + metric["fileTag"] + "*.csv"))
        if len(files) == 0:
            print("Cannot find " + metric["fileTag"] + ".csv file")
            return -1
        file = files[0]

        values = list()
        values2 = list()
        with open(file, encoding='utf=8') as csvFile:
            readCSV = csv.reader(csvFile, delimiter=',')
            for row in readCSV:
                try: 
                    values.append(float(row[metric["column"][0]]))
                    values2.append(float(row[metric["column"][1]]))
                except ValueError:
                    continue
                
        return sum(values)/len(values)-sum(values2)/len(values2)
    
    if metric["valueType"] == "MultipleXMLMean":
        
        value = list()
        pathparts = [args.folder] + (folders[metric["fileTag"]] + "*.xml").split("\\")
        files = glob.glob(os.path.join(*pathparts))
        if len(files) == 0:
            print("No xmls in folder " + folders[metric["fileTag"]])
            return -1

        for xml in files:            
            imageMeanValue = list()
            # Read xmls
            for xmlEntry in metric["xml_entry"]:
                imageMeanValue.append(get_value_from_xml(xmlEntry, xml, "float"))
                
            imageMeanValue = sum(imageMeanValue)/len(imageMeanValue)
            
            value.append(imageMeanValue)
            
        value = sum(value)/len(value)
        return(value)
        
    if "MultipleXMLMeanDelta" in metric["valueType"]:
        
        value1 = list()
        pathparts = [args.folder] + (folders[metric["fileTag"][0]]  + "*.xml").split("\\")
        files = glob.glob(os.path.join(*pathparts))
        if len(files) == 0:
            print("No xmls in folder " + folders[metric["fileTag"][0]])
            return -1
            
        for xml in files:            
            imageMeanValue = list()
            # Read xmls
            for xmlEntry in metric["xml_entry"]:
                imageMeanValue.append(get_value_from_xml(xmlEntry, xml, "float"))
                
            imageMeanValue = sum(imageMeanValue)/len(imageMeanValue)
            
            value1.append(imageMeanValue)
            
        value1 = sum(value1)/len(value1)
        
        value2 = list()
        pathparts = [args.folder] + (folders[metric["fileTag"][1]] + "*.xml").split("\\")
        files = glob.glob(os.path.join(*pathparts))
        if len(files) == 0:
            print("No xmls in folder " + folders[metric["fileTag"][1]])
            return -1
        for xml in files:            
            imageMeanValue = list()
            # Read xmls
            for xmlEntry in metric["xml_entry"]:
                imageMeanValue.append(get_value_from_xml(xmlEntry, xml, "float"))
                
            imageMeanValue = sum(imageMeanValue)/len(imageMeanValue)
            
            value2.append(imageMeanValue)
            
        value2 = sum(value2)/len(value2)
        
        if "Percent" in metric["valueType"]:
            return((value1/value2-1)*100)
        if "Absolute" in metric["valueType"]:
            return(value1-value2)
        sys.exit("Value type not supported.")
    
    # Read result csv
    files = glob.glob(os.path.join(args.folder, os.path.join(*folders[metric["fileTag"]].split("\\")), "*" + metric["fileTag"] + "*.csv"))
    if len(files) == 0:
        print("Cannot find " + metric["fileTag"] + ".csv file")
        return -1
    file = files[0]
    values = list()
    with open(file, encoding='utf=8') as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        for row in readCSV:
            try: 
                values.append(float(row[metric["column"]]))
            except ValueError:
                continue

    
    if metric["valueType"] == "fps":
        return (values[-1] - values[1])/values[0]
    if metric["valueType"] == "shootingTimeLag":
        values[2:] = [x/values[1] for x in values[2:]]
        values[2:] = [x + values[0]/1000 for x in values[2:]]
        return (sum(values[2:])/(len(values)-2))
    if metric["valueType"] == "mean":
        return sum(values)/len(values)
    sys.exit("Value type not supported.")

def analyseScores(parentScore, name):
    if name != "":
        name += " - "
    if "name" in parentScore.keys():
        name += parentScore["name"]
    childScores = parentScore["Scores"]
    for score in childScores:
        if "Scores" in score.keys():
            analyseScores(score, name)
        if "SubScores" in score.keys():
            for subScore in score["SubScores"]:
                subName = name
                if name != "":
                    subName += " - "
                subName += score["name"]
                analyseSubScores(subScore, subName)
        
        # Calculate scores
        scoreSum = 0
        weightSum = 0
        if "SubScores" in score.keys():
            for subScore in score["SubScores"]:
                subName = name
                if name != "":
                    subName += " - "
                subName += score["name"] + " - " + subScore["name"]
                scoreSum += resultDict[subName]["Score"]*subScore["SubScoreWeight"]
                weightSum += subScore["SubScoreWeight"]

        if "Scores" in score.keys():
            for entry in score["Scores"]:
                subName = ""
                if name != "":
                    subName += name + " - "
                subName += score["name"] + " - " + entry["name"]
                scoreSum += mainScoreResults[subName]*entry["weight"]
                weightSum += entry["weight"]

        mainScoreName = name
        if name != "":
            mainScoreName += " - "
        mainScoreName += score["name"]
        mainScoreResults[mainScoreName] = (scoreSum/weightSum)
        
        print(mainScoreName + " " + str(scoreSum/weightSum))


def analyseSubScores(subscore, name):
    fullname = name + " - " + subscore["name"]
    if subscore["scoreType"] == "IQScore":
        scores = list()
        filenames = list()
                
        subScoreWeight = subscore["SubScoreWeight"]
                
        # Find image xml file based on folder field
        subScoreFolder = os.path.join(args.folder, subscore["folder"])
                
        if(subScoreFolder == ""):
            return
                
        xmlFiles = glob.glob(os.path.join(subScoreFolder, "*.xml"))
                
        if len(xmlFiles) == 0:
            print("Cannot find IQAnalyzer xml for testcase " + subscore["name"])
            return
                
        allImageResults = list()
        exifDatas = list()
                
        for xmlFile in xmlFiles:
            print(xmlFile)
                    
            if "EXIF Device name" not in resultDict:
                resultDict["EXIF Device name"] = get_value_from_xml("Meta/Model", xmlFile, "string")
                    
            results = dict()
                    
            for group in subscore["Groups"]:
                groupWeight = group["groupWeight"]
                for metric in group["Metrics"]:
                            
                    if args.plotfunctions:
                        plt.clf()
                        plt.plot(metric["xp"], metric["yp"])
                        plt.title(fullname + " - " + metric["name"])
                        plt.xlabel("Metric")
                        plt.ylabel("Score")
                        figureFileName = fullname + " " + metric["name"] + ".png"
                        for char in invalid:
                            figureFileName = figureFileName.replace(char, '')
                        plt.savefig(figureFileName)
                            
                            
                    # Get value from xml file
                    if "groupFolder" not in group:
                        value = get_value_from_xml(metric["xml_entry"], xmlFile, metric["valueType"])
                    else:
                        metricFolder = os.path.join(subScoreFolder, group["groupFolder"])
                        metricFolder = os.path.join(metricFolder, metric["metricFolder"])
                        groupXmlFile = glob.glob(os.path.join(metricFolder, group["groupFilenameTag"]))[0]
                        value = get_value_from_xml(metric["xml_entry"], groupXmlFile, metric["valueType"])


                    #value = 21
                            
                    finalWeight = subScoreWeight*groupWeight*metric["weight"]*10
                            
                    # Calculate score
                    #metricScore = calculateScoreV20(value, metric["formula"], metric["LGC"], metric["HGC"], finalWeight)
                    metricScore = calculateScore(value, metric["xp"], metric["yp"], finalWeight)
                    if math.isnan(metricScore):
                        metricScore = 0
                    if math.isnan(value):
                        value = "NaN"
                            
                    results[metric["name"]] = dict()
                    results[metric["name"]]["MetricScore"] = metricScore
                    results[metric["name"]]["Weight"] = finalWeight
                    results[metric["name"]]["MetricValue"] = value
                    results[metric["name"]]["MetricGroup"] = group["groupName"]
                    
                        
            scores.append(calculateFinalSubScore(results))
            filenames.append(os.path.basename(xmlFile))
            allImageResults.append(results)
            exifDatas.append(get_value_from_xml("", xmlFile, "exif"))
                
                
        print(fullname, max(scores))
        resultDict[fullname] = dict()
        resultDict[fullname]["Score"] = max(scores)
        resultDict[fullname]["Filename"] = filenames[np.argmax(scores)]
        resultDict[fullname]["Exif"] = exifDatas[np.argmax(scores)]
        resultDict[fullname]["Metrics"] = allImageResults[np.argmax(scores)]
    if subscore["scoreType"] == "LowLight":
        lowlightLux = 10000

        luxLevels = list()
        passes = list()

        results = dict()
        subScoreWeight = subscore["SubScoreWeight"]

        # Find subfolders for each lux level 
        # Folders should follow follwing pattern YYY_XXXlux,
        # where YYY is illuminant name and XXX lux level as float
        subScoreFolder = os.path.join(args.folder, subscore["folder"])
                
        if(subScoreFolder == ""):
            return
                
        lowlightFolders = glob.glob(os.path.join(subScoreFolder, "*", ""))
                
        if len(lowlightFolders) == 0:
            print("Cannot find image subfolders for testcase " + subscore["name"])
            return

        for folder in lowlightFolders:
            lux = float(folder.split("lux")[0].split("_")[-1])
                    
            # Find all xmls in folder
            xmlFiles = glob.glob(os.path.join(folder, "*.xml"))
            for xmlFile in xmlFiles:
                passed = True
                for metric in subscore["Metrics"]:
                    metricPass = True
                    value = get_value_from_xml(metric["xml_entry"], xmlFile, metric["valueType"])
                    if(metric["limitType"] == "min"):
                        if(value < metric["limit"]):
                            print("Low Light Metric " + metric["name"] + " failed at " + str(lux) + "lux")
                            metricPass = False
                    elif(metric["limitType"] == "max"):
                        if(value > metric["limit"]):
                            print("Low Light Metric " + metric["name"] + " failed at " + str(lux) + "lux")
                            metricPass = False
                    elif(metric["limitType"] == "relativeMin"):
                        # Get highest illuminance as reference
                        highestIlluminance = 0
                        highestFolder = ""
                        for folder2 in lowlightFolders:
                            lux2 = float(folder2.split("lux")[0].split("_")[-1])
                            if lux2 > highestIlluminance:
                                highestIlluminance = lux2
                                highestFolder = folder2
                        refXmlFiles = glob.glob(os.path.join(highestFolder, "*.xml"))
                        refValue = 0
                        for refXmlFile in refXmlFiles:
                            refValue += get_value_from_xml(metric["xml_entry"], refXmlFile, metric["valueType"])
                        refValue /= len(refXmlFiles)
                        value = value/refValue
                        if(value < metric["limit"]):
                            print("Low Light Metric " + metric["name"] + " failed at " + str(lux) + "lux")
                            metricPass = False
                    if math.isnan(value):
                        value = "NaN"
                    results[str(lux) + "lux " + metric["name"]] = dict()
                    results[str(lux) + "lux " + metric["name"]]["MetricScore"] = metricPass
                    results[str(lux) + "lux " + metric["name"]]["Weight"] = 0
                    results[str(lux) + "lux " + metric["name"]]["MetricValue"] = value
                    results[str(lux) + "lux " + metric["name"]]["MetricGroup"] = subscore["name"]
                    if metricPass == False:
                        passed = False
                luxLevels.append(lux)
                passes.append(passed)
        uniqueLuxes = sorted(set(luxLevels))
        uniquePasses = list()
        for luxLevel in uniqueLuxes:
            indices = [i for i, x in enumerate(luxLevels) if x == luxLevel]
            passed = False
            for i in indices:
                if passes[i] == True:
                    passed = True
            uniquePasses.append(passed)
        for i in reversed(range(len(uniquePasses))):
            if uniquePasses[i] == False:
                break
            lowlightLux = uniqueLuxes[i]

        lowlightScore = calculateScore(lowlightLux, subscore["xp"], subscore["yp"], 1)
        if args.plotfunctions:
            plt.clf()
            plt.plot(subscore["xp"], subscore["yp"])
            plt.title(fullname)
            plt.xlabel("Metric")
            plt.ylabel("Score")
            figureFileName = fullname + ".png"
            for char in invalid:
                figureFileName = figureFileName.replace(char, '')
            plt.savefig(figureFileName)
        print(fullname, lowlightScore*100)
        resultDict[fullname] = dict()
        resultDict[fullname]["Score"] = lowlightScore*100
        resultDict[fullname]["LowLightLux"] = lowlightLux
        resultDict[fullname]["Metrics"] = results    
    if subscore["scoreType"] == "Performance":
        # Perfomance scores
        subScoreWeight = subscore["SubScoreWeight"]
        results = dict()
        for group in subscore["Groups"]:
            groupWeight = group["groupWeight"]
            for metric in group["Metrics"]:
                if args.plotfunctions:
                    plt.clf()
                    plt.plot(metric["xp"], metric["yp"])
                    plt.title(fullname + " - " + metric["name"])
                    plt.xlabel("Metric")
                    plt.ylabel("Score")
                    figureFileName = fullname + " " + metric["name"] + ".png"
                    for char in invalid:
                        figureFileName = figureFileName.replace(char, '')
                    plt.savefig(figureFileName)
                value = calculatePerformanceMetric(metric, subscore["Folders"])
                finalWeight = subScoreWeight*groupWeight*metric["weight"]*10

                if value == -1:
                    metricScore = 0
                else:
                    metricScore = calculateScore(value, metric["xp"], metric["yp"], finalWeight)

                if math.isnan(metricScore):
                    metricScore = 0
                if math.isnan(value):
                    value = "NaN"
                        
                        
                results[metric["name"]] = dict()
                results[metric["name"]]["MetricScore"] = metricScore
                results[metric["name"]]["Weight"] = finalWeight
                results[metric["name"]]["MetricValue"] = value
                results[metric["name"]]["MetricGroup"] = group["groupName"]
                #print(metric["name"], metricScore, finalWeight, value)
        perfScore = calculateFinalSubScore(results)
        resultDict[fullname] = dict()
        resultDict[fullname]["Score"] = perfScore
        resultDict[fullname]["Metrics"] = results  
                    
        print(fullname, perfScore)

    # Calculate group scores
    groupScores = dict()
    scoreName = fullname
    if subscore["scoreType"] == "LowLight":
        return
    if scoreName not in resultDict.keys():
        return
    for group in subscore["Groups"]:
        groupScore = 0
        groupWeight = 0
        for metric in resultDict[scoreName]["Metrics"]:
            if group["groupName"] in resultDict[scoreName]["Metrics"][metric]["MetricGroup"]:
                groupWeight += resultDict[scoreName]["Metrics"][metric]["Weight"]
                groupScore += resultDict[scoreName]["Metrics"][metric]["MetricScore"]
        points = int(np.floor(groupScore/(groupWeight/11)-1E-10))
        points = np.max([points, 0])

        groupScores[group["groupName"]] = float(points)
    resultDict[fullname]["GroupScores"] = groupScores 

if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])
    
    if args.plotfunctions:
        print("Saving plots of metric to score functions.")
    
    # Find config file
    if not os.path.isfile(args.config):
        sys.exit("Config file does not exist.")
    
    
    with open(args.config, 'r') as f:
        config = json.load(f)
    
    resultDict = dict()
    mainScoreResults = dict()

    analyseScores(config, "")
        

    # Calculate final VCX score
    scoreSum = 0
    weightSum = 0
    for mainscore in config["Scores"]:
        scoreSum += mainScoreResults[mainscore["name"]] * mainscore["weight"]
        weightSum += mainscore["weight"]
    VCXScore = (scoreSum/weightSum)
    print("VCX Score " + str(VCXScore))

    # Ask additional info from user
    # Device name
    deviceName = input("Device name: ")
    # IMEI
    IMEI = input("IMEI: ")
    # OS Version
    OSVersion = input("OS Version: ")
    # Firmware version
    FWVersion = input("Firmware Version: ")

    # Make a json with all results
    jsonDict = dict()
    jsonDict["VCX Version"] = config["VCX Version"]
    jsonDict["Test Date"] = date.today().strftime("%d.%m.%Y")
    jsonDict["Device Name"] = deviceName
    jsonDict["IMEI"] = IMEI
    jsonDict["OS Version"] = OSVersion
    jsonDict["Firmware Version"] = FWVersion
    jsonDict["VCXScore"] = VCXScore
    jsonDict["MainScores"] = mainScoreResults
    jsonDict["SubScores"] = resultDict
    resultJsonFilename = os.path.join(args.folder, "vcxResults.json")
    with open(resultJsonFilename, 'w') as outfile:
        json.dump(jsonDict, outfile, indent=2, separators=(',', ': '))
    